## Getting Started

### Clone the repository
```bash
git clone https://hajama4@bitbucket.org/hajama4/userapi.git
```

### Change directory
```bash
cd userapi
```

### Command to launch docker
```bash
docker-compose up -d --build
```

### For windows to launch in command line
```bash
 docker exec -it userapi_service_php bash
```
### install vendor inside opened php container
```bash
 composer install
```

### create .env file and copy contents from .env.dev
```bash
 cp .env.dev .env
```