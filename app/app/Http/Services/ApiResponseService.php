<?php declare(strict_types=1);

namespace App\Http\Services;

use Illuminate\Http\JsonResponse;

class ApiResponseService
{
    public function successResponse($data, $statusCode = Response::HTTP_OK): JsonResponse
    {
        return response()->json(['success' => $data, 'success_code' => $statusCode], $statusCode);
    }
    public function errorResponse($errorMessage, $statusCode): JsonResponse
    {
        return response()->json(['error' => $errorMessage, 'error_code' => $statusCode], $statusCode);
    }
}
