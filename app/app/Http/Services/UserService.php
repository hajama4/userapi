<?php declare(strict_types=1);

namespace App\Http\Services;

use App\Http\Repositories\UserRepository;
use App\Http\Requests\User\RegisterUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\User;

class UserService
{
    protected UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getUsers(): ?array
    {
        return $this->userRepository->getAll();
    }

    public function setUser(RegisterUserRequest $request): User
    {
        return $this->userRepository->setUser($request);
    }

    public function updateUser(UpdateUserRequest $request, User $user): User
    {
        return $this->userRepository->updateUser($request, $user);
    }

    public function deleteUser(User $user)
    {
        return $this->userRepository->deleteUser($user);
    }
}
