<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Services\ApiResponseService;
use App\Http\Services\UserService;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    protected ApiResponseService $apiResponseService;
    protected UserService $userService;

    public function __construct(ApiResponseService $apiResponseService, UserService $userService)
    {
        $this->apiResponseService = $apiResponseService;
        $this->userService = $userService;
    }

    public function index(): JsonResponse
    {
        $user = $this->userService->getUsers();

        return $this->apiResponseService->successResponse($user, '200');
    }

    public function show(User $user): JsonResponse
    {
        return $this->apiResponseService->successResponse($user, '200');
    }

    public function update(UpdateUserRequest $request, User $user): JsonResponse
    {
        $request['password']=Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);

        $response = $this->userService->updateUser($request, $user);

        return $this->apiResponseService->successResponse($response, '200');
    }

    public function destroy(User $user): JsonResponse
    {
        $response = $this->userService->deleteUser($user);

        return $this->apiResponseService->successResponse($response, '200');
    }
}
