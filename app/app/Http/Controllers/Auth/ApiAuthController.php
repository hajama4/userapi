<?php declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\User\RegisterUserRequest;
use App\Http\Services\ApiResponseService;
use App\Http\Services\UserService;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ApiAuthController
{
    protected UserService $userService;
    protected ApiResponseService $apiResponseService;

    public function __construct(UserService $userService, ApiResponseService $apiResponseService)
    {
        $this->userService = $userService;
        $this->apiResponseService = $apiResponseService;
    }

    public function register(RegisterUserRequest $request): JsonResponse
    {
        $request['password']=Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);

        $user = $this->userService->setUser($request);

        $token = $user->createToken('Access Token');

        $response = ['user' => $user, 'token' => $token];

        return $this->apiResponseService->successResponse($response, '201');
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Access Token');
                $response = ['token' => $token];

                return $this->apiResponseService->successResponse($response, '200');
            } else {
                $response = ["message" => "Password mismatch"];

                return $this->apiResponseService->errorResponse($response, '422');
            }
        } else {
            $response = ["message" =>'User does not exist'];

            return $this->apiResponseService->errorResponse($response, '422');
        }
    }

    public function logout(Request $request)
    {
        $tokens = $request->user()->tokens();
        $tokens->delete();
        $response = ['message' => 'You have been successfully logged out!'];

        return $this->apiResponseService->successResponse($response, '200');
    }
}
