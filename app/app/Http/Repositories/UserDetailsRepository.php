<?php declare(strict_types=1);

namespace App\Http\Repositories;

use App\Http\Requests\User\RegisterUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\User;
use App\Models\UserDetails;

class UserDetailsRepository
{
    public function create(RegisterUserRequest $request, User $user)
    {
        $userDetails = new UserDetails();
        $userDetails->address = $request['address'];

        $user->userDetails()->save($userDetails);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $userDetail = $user->userDetails()->getResults();
        $userDetail->address = $request['address'];
        $userDetail->update();
    }
}
