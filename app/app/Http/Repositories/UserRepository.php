<?php declare(strict_types=1);

namespace App\Http\Repositories;

use App\Http\Requests\User\RegisterUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\User;

class UserRepository
{
    private UserDetailsRepository $userDetailsRepo;

    public function __construct(UserDetailsRepository $userDetailsRepo)
    {
        $this->userDetailsRepo = $userDetailsRepo;
    }

    public function getAll(): ?array
    {
        return User::query()->get()->toArray();
    }

    public function setUser(RegisterUserRequest $request): User
    {
        $user = User::create($request->toArray());

        if (!is_null($request['address'])) {
            $this->userDetailsRepo->create($request, $user);
        }

        return $user;
    }

    public function updateUser(UpdateUserRequest $request, User $user): User
    {
        $user->update($request->toArray());

        if (!is_null($request['address'])) {
            $this->userDetailsRepo->update($request, $user);
        }

        return $user;
    }

    public function deleteUser(User $user)
    {
        $deleted = $user->delete();

        if ($deleted) {
            return 'true';
        }

        return 'false';
    }
}
